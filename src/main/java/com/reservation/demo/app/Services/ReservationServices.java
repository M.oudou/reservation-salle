package com.reservation.demo.app.Services;

import java.util.List;

import com.reservation.demo.app.Entities.Equipment;
import com.reservation.demo.app.Entities.Reservation;
import com.reservation.demo.app.Repository.EquipmentRepository;
import com.reservation.demo.app.Repository.ReservationRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReservationServices {

    @Autowired
    private ReservationRepository reservationRepository;

    @Autowired
    private EquipmentServices equipmentServices;

    public List<Reservation> getAll() {
        return reservationRepository.findAll();
    }

    public Reservation getById(int id) {
        return reservationRepository.findById(id).orElse(null);
    }

    public Reservation createOrUpdate(Reservation reservation) throws Exception {
        return reservationRepository.save(reservation);
    }

    public Boolean disponibleClassroom(Reservation reservation) {

        List<Reservation> result = (List<Reservation>) reservationRepository.disponibleClass(Integer.parseInt(reservation.getClassRoomId()), reservation.getDateReservation(), reservation.getStart(), reservation.getEnd());

        return result.size()>0 ? false: true;
    }

    public Boolean disponibleEquipment(Reservation reservation) {

        Boolean response = false;
        Equipment equipment = equipmentServices.getById(Integer.parseInt(reservation.getEquipmentId()));

        if (equipment != null) {
            response = true;
            List<Reservation> result = (List<Reservation>) reservationRepository.disponibleEquip(Integer.parseInt(reservation.getEquipmentId()), reservation.getDateReservation(), reservation.getStart(), reservation.getEnd());
            System.out.print(result);

            if (result.size()> 0) {
                response = false;   
            }
        }

        return  response;
    }
    
}
