package com.reservation.demo.app.Services;

import com.reservation.demo.app.Repository.EquipmentRepository;
import com.reservation.demo.app.Repository.InterfaceEquipment;
import com.reservation.demo.app.Entities.Equipment;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EquipmentServices {

    @Autowired
    private EquipmentRepository equipmentRepository;

    public List<Equipment> getAll() {
        return equipmentRepository.findAll();
    }

    public Equipment getById(int id) {
        return equipmentRepository.findById(id).orElse(null);
    }

    public Equipment createOrUpdate(Equipment reservation) throws Exception {
        return equipmentRepository.save(reservation);
    }

    public List<InterfaceEquipment> planning()  {
        return equipmentRepository.Planning();
    }
    
}
