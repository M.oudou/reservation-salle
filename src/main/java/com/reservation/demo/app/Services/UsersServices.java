package com.reservation.demo.app.Services;

import java.util.List;

import com.reservation.demo.app.Entities.Users;
import com.reservation.demo.app.Repository.UsersRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsersServices {

    @Autowired
    private UsersRepository usersRepository;

    public List<Users> getAll() {
        return usersRepository.findAll();
    }

    public Users getById(int id) {
        return usersRepository.findById(id).orElse(null);
    }

    public Users createOrUpdate(Users user) throws Exception {

        return usersRepository.save(user);
    }

    
}
