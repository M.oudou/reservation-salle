package com.reservation.demo.app.Services;

import java.util.List;

import com.reservation.demo.app.Entities.Classroom;
import com.reservation.demo.app.Repository.ClassroomRepository;
import com.reservation.demo.app.Repository.InterfaceClassroom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClassroomServices {

    @Autowired
    private ClassroomRepository classroomRepository;

    public List<Classroom> getAll() {
        return classroomRepository.findAll();
    }

    public Classroom getById(int id) {
        return classroomRepository.findById(id).orElse(null);
    }

    public Classroom createOrUpdate(Classroom reservation) throws Exception {
        return classroomRepository.save(reservation);
    }

    public List<InterfaceClassroom> planning()  {
        return classroomRepository.Planning();
    }

    //public List<Classroom>
    /*
    planning
    [
        {
            id: ,
            salle: ,
            place: ,
            planning: [
                {
                    date: ,
                    start: ,
                    end: ,
                }
            ],
            photo: 
        }
    ]

    */

    /*

    verifier disponibiliter
    
    data: {
        id_salle: ,
        start: ,
        end: ,
        date:
    }

    return {
        disponible: true or false
    }

    */
    
}
