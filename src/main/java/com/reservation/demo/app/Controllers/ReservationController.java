package com.reservation.demo.app.Controllers;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.reservation.demo.app.Entities.Reservation;
import com.reservation.demo.app.Entities.Users;
import com.reservation.demo.app.Services.ReservationServices;
import com.reservation.demo.app.Services.UsersServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;

@CrossOrigin
@RestController
@Tag(name = "Reservation", description = "")
public class ReservationController {
    @Autowired
    private ReservationServices reservationServices;

    @Autowired
    private UsersServices usersServices;

    @GetMapping("/reservation")
    public List<Reservation> getAll(){
        return reservationServices.getAll();
    }
    
    @GetMapping("/reservation/{reservation_id}")
    public Reservation getById(@PathVariable int reservation_id){
        return reservationServices.getById(reservation_id);
    }

    @PostMapping("/reservation/{type}")
    public Object createReservation(@RequestBody Reservation reservation,  @PathVariable int type) throws Exception {
        //System.out.println("controller " + reservation.getDateReservation());
        Object result = new Object();
        boolean dispo = false;


        if (type == 0) {
            dispo = reservationServices.disponibleClassroom(reservation);
        } else if (type == 1) {
            dispo = reservationServices.disponibleEquipment(reservation);   
        } else {
            return result = Collections.singletonMap("message", "preciser le type de reservation");
        }

        //System.out.println("dispo " + dispo);

        if (reservation.getUserId() == null) {
            return result = Collections.singletonMap("message", "le champs userId manque a la requete");
        }

        Users user = usersServices.getById(Integer.parseInt(reservation.getUserId()));
        // System.out.println("type " + user.getTypeUser());
        
        if (reservation.getUserId() == null || user == null) {
            return result = Collections.singletonMap("message", "l'utilisateur n'exite pas");
        } if (Integer.parseInt(user.getTypeUser()) != 1) {
            return result = Collections.singletonMap("message", "Vous n'avez pas le droit");
        }

        if (!dispo) {
            result = Collections.singletonMap("message", "pas disponible"); 
        } else {
            result = reservationServices.createOrUpdate(reservation);
        }
        return result;
    }

    /*@PostMapping("/disponibilite/{type}")
    public Map<String, String> disponibiliter(@RequestBody Reservation reservation, @PathVariable String type) throws Exception {
        Map<String, String> result = Collections.singletonMap("message", "Cet type n'existe pas");
        boolean dispo = reservationServices.disponibleClassroom(reservation);
        if (!dispo) {
            result = Collections.singletonMap("message", "pas disponible"); 
        } //else if (type == "salle") {} 
        else {
            result = Collections.singletonMap("message", "dispo"); 
        }
        // Reservation result = reservationServices.createOrUpdate(reservation);
        return result;
    }*/

    @PostMapping("/reservation/disponibilite/salle")
    @Tag(name = "Vérification disponibilité", description = "")
    public Object getDispo(@RequestBody Reservation reservation){
        Boolean dispo = reservationServices.disponibleClassroom(reservation);
        Object result = new Object();

        if (!dispo) {
             result = Collections.singletonMap("message", "Cette salle n'est pas disponible");

        } else {
             result = Collections.singletonMap("message", "Cette salle est disponible");

        }

        return result;
    }

    @PostMapping("/reservation/disponibilite/equipement")
    @Tag(name = "Vérification disponibilité", description = "")
    public Object getDispoEquip(Reservation reservation){
        return reservationServices.disponibleEquipment(reservation);
    }


    @PutMapping("/reservation/{reservation_id}")
    public Reservation update(@RequestBody Reservation reservation, @PathVariable Integer reservation_id) throws Exception {  
      return reservationServices.createOrUpdate(reservation);
    }
}
