package com.reservation.demo.app.Controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.reservation.demo.app.Entities.Classroom;
import com.reservation.demo.app.Repository.InterfaceClassroom;
import com.reservation.demo.app.Services.ClassroomServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.tags.Tag;

@CrossOrigin
@RestController
@Tag(name = "Salle", description = "")
public class ClassroomController {

    @Autowired
    private ClassroomServices classroomServices;

    @GetMapping("/salle")
    public List<Classroom> getAll(){
        return classroomServices.getAll();
    }
    
    
    @GetMapping("/planning/salle")
    @Tag(name = "Planning des reservations", description = "")
    public List<InterfaceClassroom> getPlanning(){
        return classroomServices.planning();
    }



    @GetMapping("/salle/{salle_id}")
    public Classroom getById(@PathVariable int salle_id){
        return classroomServices.getById(salle_id);
    }

    @PostMapping("/salle")
    public Classroom create(@RequestBody Classroom salle) throws Exception {
        Classroom result = classroomServices.createOrUpdate(salle);
        return result;
    }

    @PutMapping("/salle/{salle_id}")
    public Classroom update(@RequestBody Classroom salle, @PathVariable Integer salle_id) throws Exception {  
      return classroomServices.createOrUpdate(salle);
    }
    
}
