package com.reservation.demo.app.Controllers;

import java.util.List;

import com.reservation.demo.app.Entities.Equipment;
import com.reservation.demo.app.Repository.InterfaceEquipment;
import com.reservation.demo.app.Services.EquipmentServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;

@CrossOrigin
@RestController
@Tag(name = "Equipement", description = "")
public class EquipmentController {
    @Autowired
    private EquipmentServices equipmentServices;

    @GetMapping("/equipement")
    public List<Equipment> getAll(){
        return equipmentServices.getAll();
    }

    @GetMapping("/planning/equipment")
    @Tag(name = "Planning des reservations", description = "")
    public List<InterfaceEquipment> getPlanning(){
        return equipmentServices.planning();
    }
    
    @GetMapping("/equipement/{equipement_id}")
    public Equipment getById(@PathVariable int equipement_id){
        return equipmentServices.getById(equipement_id);
    }

    @PostMapping("/equipement")
    public Equipment create(@RequestBody Equipment equipement) throws Exception {
        Equipment result = equipmentServices.createOrUpdate(equipement);
        return result;
    }

    @PutMapping("/equipement/{equipement_id}")
    public Equipment update(@RequestBody Equipment equipement, @PathVariable Integer equipement_id) throws Exception {  
      return equipmentServices.createOrUpdate(equipement);
    }
}
