package com.reservation.demo.app.Controllers;

import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.reservation.demo.app.Entities.Users;
import com.reservation.demo.app.Services.UsersServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.Collections;
import java.util.Map;

@CrossOrigin
@RestController
@Tag(name = "Users", description = "")
public class UsersController {

    @Autowired
    private UsersServices usersServices;

    @GetMapping("/")
	public Map<String, Object> greeting() {
		return Collections.singletonMap("message", "Hello, World");
	}

    @GetMapping("/users")
    public List<Users> getAll(){
        return usersServices.getAll();
    }
    
    @GetMapping("/users/{users_id}")
    public Users getById(@PathVariable int users_id){
        return usersServices.getById(users_id);
    }

    @PostMapping("/users")
    public Object create(@RequestBody Users user) throws Exception {
        Object result = new Object();
        if (Integer.parseInt(user.getTypeUser()) == 0 || Integer.parseInt(user.getTypeUser()) == 1) {
            result = usersServices.createOrUpdate(user);
        } else {
            result = Collections.singletonMap("message", "le type d'utilisateur n'existe pas");
        }

        
        return result;
    }

    @PutMapping("/users/{users_id}")
    public Users update(@RequestBody Users user, @PathVariable Integer users_id) throws Exception {  
      return usersServices.createOrUpdate(user);
    }

}
