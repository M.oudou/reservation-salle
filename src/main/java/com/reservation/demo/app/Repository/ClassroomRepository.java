package com.reservation.demo.app.Repository;

import java.util.ArrayList;

import com.reservation.demo.app.Entities.Classroom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ClassroomRepository extends JpaRepository<Classroom, Integer> {

    String requete0 = "SELECT reservation.*, salle.*, users.fullname FROM reservation, salle, users WHERE  salle.classroom_id = reservation.class_room_id AND users.user_id = reservation.user_id AND (reservation.date_reservation > NOW() OR reservation.date_reservation = NOW()) ORDER BY reservation.date_reservation";

    @Query(value = requete0, nativeQuery = true)
    public ArrayList<InterfaceClassroom> Planning();

    /*String requete1 = "SELECT reservation.*, salle.*, users.fullname FROM reservation, salle, users WHERE salle.classroom_id = reservation.class_room_id AND users.user_id = reservation.user_id";
    @Query(value = requete1, nativeQuery = true)
    public ArrayList<InterfaceClassroom> disponibilite();*/
    
}
