package com.reservation.demo.app.Repository;

import java.util.ArrayList;

import com.reservation.demo.app.Entities.Equipment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface EquipmentRepository extends  JpaRepository<Equipment, Integer> {
 
    String requete = "SELECT reservation.*, materiel.equipment_name, users.fullname FROM reservation, materiel, users WHERE materiel.equipment_id = reservation.equipment_id AND users.user_id = reservation.user_id AND (reservation.date_reservation > NOW() OR reservation.date_reservation = NOW()) ORDER BY reservation.date_reservation";
    @Query(value = requete, nativeQuery = true)
    public ArrayList<InterfaceEquipment> Planning();
    
}
