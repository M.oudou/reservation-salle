package com.reservation.demo.app.Repository;
import com.reservation.demo.app.Entities.Users;

import org.springframework.data.jpa.repository.JpaRepository;


public interface UsersRepository extends JpaRepository<Users, Integer> {
    
}
