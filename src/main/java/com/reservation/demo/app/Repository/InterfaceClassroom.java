package com.reservation.demo.app.Repository;

import java.sql.Time;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public interface InterfaceClassroom {

    public Integer getReservation_id();
    public Integer getClassroom_id();
    public String getName_room();
    public String getNumber_place();
    public String getFullname();

    //public byte[] getRoomPhoto();number_place

    @JsonFormat(pattern = "yyyy/MM/dd")
    public Date getDate_reservation();
    public Time getStart();
    public Time getEnd();


    /*
        private int classroomId;
    private String nameRoom;
    private String numberPlace;
    @Lob
    private byte[] roomPhoto;
            {
            id: ,
            salle: ,
            place: ,
            planning: [
                {
                    date: ,
                    start: ,
                    end: ,
                }
            ],
            photo: 
        }
    */

}
