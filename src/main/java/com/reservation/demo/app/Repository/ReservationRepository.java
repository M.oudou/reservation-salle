package com.reservation.demo.app.Repository;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.reservation.demo.app.Entities.Reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ReservationRepository extends JpaRepository<Reservation, Integer> {
    
    /*public String requete = "SELECT * FROM reservation WHERE reservation.date_reservation = :date  AND " +
    // start
    "((TIMEDIFF(reservation.start, :start) < 0  and TIMEDIFF(reservation.end, :start) > 0) OR " + 
    // end
    "(TIMEDIFF(reservation.start, :end) < 0  and TIMEDIFF(reservation.end, :end) > 0))";*/

    public String requete0 = "SELECT * FROM reservation WHERE reservation.class_room_id = :classroom_id AND reservation.date_reservation = :date  AND " +
    "((TIMEDIFF(reservation.start, :start) < 0 OR TIMEDIFF(reservation.start, :start) = 0)  and (TIMEDIFF(reservation.end, :start) > 0 OR TIMEDIFF(reservation.end, :start) = 0)) AND " +
    "((TIMEDIFF(reservation.start, :end) < 0 OR TIMEDIFF(reservation.start, :end) = 0)  and (TIMEDIFF(reservation.end, :end) > 0 OR TIMEDIFF(reservation.end, :end) = 0))";


    public String requete1 = "SELECT * FROM reservation WHERE reservation.equipment_id = :equipment_id AND reservation.date_reservation = :date  AND " +
    "((TIMEDIFF(reservation.start, :start) < 0 OR TIMEDIFF(reservation.start, :start) = 0)  and (TIMEDIFF(reservation.end, :start) > 0 OR TIMEDIFF(reservation.end, :start) = 0)) AND " +
    "((TIMEDIFF(reservation.start, :end) < 0 OR TIMEDIFF(reservation.start, :end) = 0)  and (TIMEDIFF(reservation.end, :end) > 0 OR TIMEDIFF(reservation.end, :end) = 0))";

    /*
    SELECT * FROM reservation WHERE reservation.class_room_id ="" AND reservation.date_reservation = "2022/02/12"  AND 
(
((TIMEDIFF(reservation.start, "10:40:28") < 0 OR TIMEDIFF(reservation.start, "10:40:28") = 0)  and (TIMEDIFF(reservation.end, "10:40:28") > 0 OR TIMEDIFF(reservation.end, "10:40:28") = 0))
OR
((TIMEDIFF(reservation.start, "14:40:28") < 0 OR TIMEDIFF(reservation.start, "14:40:28") = 0)  and (TIMEDIFF(reservation.end, "14:40:28") > 0 OR TIMEDIFF(reservation.end, "14:40:28") = 0))
)
    */

    @Query(value = requete0, nativeQuery = true)
    public Collection<Reservation> disponibleClass(@Param("classroom_id") int classroom_id, @Param("date") Date date, @Param("start") Time start, @Param("end") Time end);

    
    @Query(value = requete1, nativeQuery = true)
    public ArrayList<Reservation> disponibleEquip(@Param("equipment_id") int equipment_id, @Param("date") Date date, @Param("start") Time start, @Param("end") Time end);
}
