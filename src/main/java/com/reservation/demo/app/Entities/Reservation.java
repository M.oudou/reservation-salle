package com.reservation.demo.app.Entities;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;


@Entity
@Table(name = "reservation")
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int reservationId;

    @JsonFormat(pattern = "yyyy/MM/dd")
    private Date dateReservation;

    @JsonFormat(pattern = "HH:mm:ss")
    private Time start;

    @JsonFormat(pattern = "HH:mm:ss")
    private Time end;
    

    @Nullable
    private String libelle;

    private String userId;
    @Nullable
    private String classRoomId;
    @Nullable
    private String equipmentId;
    
    @Override
    public String toString() {
        return "Reservation [classRoomId=" + classRoomId + ", dateReservation=" + dateReservation + ", end=" + end
                + ", equipmentId=" + equipmentId + ", libelle=" + libelle + ", reservationId=" + reservationId
                + ", start=" + start + ", userId=" + userId + "]";
    }


    public Reservation() {
    }

    public Reservation(int reservationId, Date dateReservation, Time start, Time end, String libelle, String userId,
            String classRoomId, String equipmentId) {
        this.reservationId = reservationId;
        this.dateReservation = dateReservation;
        this.start = start;
        this.end = end;
        this.libelle = libelle;
        this.userId = userId;
        this.classRoomId = classRoomId;
        this.equipmentId = equipmentId;
    }

    /**
     * @return int return the reservationId
     */
    public int getReservationId() {
        return reservationId;
    }

    /**
     * @param reservationId the reservationId to set
     */
    public void setReservationId(int reservationId) {
        this.reservationId = reservationId;
    }

    /**
     * @return Date return the dateReservation
     */
    public Date getDateReservation() {
        return dateReservation;
    }

    /**
     * @param dateReservation the dateReservation to set
     */
    public void setDateReservation(Date dateReservation) {
        this.dateReservation = dateReservation;
    }

    /**
     * @return Time return the start
     */
    public Time getStart() {
        return start;
    }

    /**
     * @param start the start to set
     */
    public void setStart(Time start) {
        this.start = start;
    }

    /**
     * @return Time return the end
     */
    public Time getEnd() {
        return end;
    }

    /**
     * @param end the end to set
     */
    public void setEnd(Time end) {
        this.end = end;
    }

    /**
     * @return String return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * @param libelle the libelle to set
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /**
     * @return String return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return String return the classRoomId
     */
    public String getClassRoomId() {
        return classRoomId;
    }

    /**
     * @param classRoomId the classRoomId to set
     */
    public void setClassRoomId(String classRoomId) {
        this.classRoomId = classRoomId;
    }

    /**
     * @return String return the equipmentId
     */
    public String getEquipmentId() {
        return equipmentId;
    }

    /**
     * @param equipmentId the equipmentId to set
     */
    public void setEquipmentId(String equipmentId) {
        this.equipmentId = equipmentId;
    }

}
