package com.reservation.demo.app.Entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "salle")
public class Classroom {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int classroomId;
    private String nameRoom;
    private String numberPlace;
    public Classroom() {
    }
    //@Lob
    //private byte[] roomPhoto;
    public Classroom(int classroomId, String nameRoom, String numberPlace) {
        this.classroomId = classroomId;
        this.nameRoom = nameRoom;
        this.numberPlace = numberPlace;
    }
    


    /**
     * @return int return the classroomId
     */
    public int getClassroomId() {
        return classroomId;
    }

    /**
     * @param classroomId the classroomId to set
     */
    public void setClassroomId(int classroomId) {
        this.classroomId = classroomId;
    }

    /**
     * @return String return the nameRoom
     */
    public String getNameRoom() {
        return nameRoom;
    }

    /**
     * @param nameRoom the nameRoom to set
     */
    public void setNameRoom(String nameRoom) {
        this.nameRoom = nameRoom;
    }

    /**
     * @return String return the numberPlace
     */
    public String getNumberPlace() {
        return numberPlace;
    }

    /**
     * @param numberPlace the numberPlace to set
     */
    public void setNumberPlace(String numberPlace) {
        this.numberPlace = numberPlace;
    }

}
