package com.reservation.demo.app.Entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;


@Entity
@Table(name = "materiel")
public class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int equipmentId;
    private String equipmentName;
    private String equipmentDescript;

    //@Lob
    //private byte[] equipmentPhoto;

    public Equipment(int equipmentId, String equipmentName, String equipmentDescript) {
        this.equipmentId = equipmentId;
        this.equipmentName = equipmentName;
        this.equipmentDescript = equipmentDescript;
    }

    public Equipment() {
    }

    public int getEquipmentId() {
        return equipmentId;
    }

    public void setEquipmentId(int equipmentId) {
        this.equipmentId = equipmentId;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getEquipmentDescript() {
        return equipmentDescript;
    }

    public void setEquipmentDescript(String equipmentDescript) {
        this.equipmentDescript = equipmentDescript;
    }


    
}
